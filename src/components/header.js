import React, {Component} from 'react';
import {
    View,
    Image,
    StatusBar,
    Text,
    TouchableOpacity,
    Easing,
    Animated,
    Dimensions,
    AppRegistry,
    StyleSheet,
} from 'react-native';

import IconSimpleLine from 'react-native-vector-icons/SimpleLineIcons';
import IconAntDesign from 'react-native-vector-icons/AntDesign';

const { width } = Dimensions.get('window')

export default class Index extends Component {

    constructor(props) {
        super(props);
        this.animatedValue2 = new Animated.Value(0)
        this.state = {
            touchMenu:false
        }
    }

    componentDidMount () {
        
    }
    
      animate () {
        this.animatedValue2.setValue(0)
        const createAnimation = function (value, duration, easing, delay = 100) {
          return Animated.timing(
           value,
            {
              toValue: 1,
              duration,
              easing,
              delay,
              useNativeDriver:true
            }
          )
        }
        Animated.parallel([
          createAnimation(this.animatedValue2, 90, Easing.ease, 90)
        ]).start()
      }

    _touchMenu = () =>{
        // var a ='';
        this.animate();

        if(this.state.touchMenu == true ){
          this.setState({touchMenu:false})
        }else{
          this.setState({touchMenu:true})
        }
    }

    render() {

        const{menu, cart, titleCart, Title} = this.props;

        const spinText = this.animatedValue2.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '720deg']
          })

        return (
            <View style={{ minHeight:59, backgroundColor:"transparent", flexDirection:'row', justifyContent:'space-between', alignItems: 'center' }}>
               <View style={{ marginLeft:10 }}>
                    {menu ?  
                        <Animated.View
                            style={{ transform: [{rotate: spinText}] }}
                        >
                            <TouchableOpacity
                                onPress={()=>{
                                    this._touchMenu()
                                    this.setState({
                                        touchMenu:!this.state.touchMenu
                                    })
                                }}
                            >
                                

                                {this.state.touchMenu ? 
                                    <IconAntDesign name={"close"} style={{fontSize: 30 }} />
                                :
                                    // <IconSimpleLine name='magnifier' style={{ color: '#ffff', fontSize: 22 }} />
                                    <IconSimpleLine
                                        name={"menu"}
                                        size={25}
                                    />
                                }
                            </TouchableOpacity>
                        </Animated.View>
                    :
                        <View/>
                    }
               </View>

               <View>
                   {Title ?
                    <Text>{Title}</Text> 
                   :
                    <View/>
                   }
               </View>

               <View style={{ marginRight:15 }}>
                    {cart ? 
                        <View>
                            <View style={{ position:"absolute", backgroundColor:"red", borderRadius:20, paddingLeft:4, paddingRight:4, right:-9, top:-3 }}>
                                {titleCart ? 
                                    <Text style={{ fontSize:10, color:"#ffff", fontWeight:'bold' }}>{titleCart ? titleCart : ""}</Text>
                                :
                                    <View/>
                                }
                            </View>
                            <IconSimpleLine
                                name={"basket"}
                                size={25}
                            />
                        </View>
                    :
                        <View/>
                    }
               </View>

               
            </View>
        );
    }
}