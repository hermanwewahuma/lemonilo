

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react'
import {
    View,
    Text,
    StyleSheet,
    BackHandler,
    Platform,
    NetInfo,
    ToastAndroid
} from 'react-native'
//import CardStackStyleInterpolator from 'react-navigation/src/views/CardStack/CardStackStyleInterpolator'
import {
    Scene,
    Router,
    Actions,
    Reducer,
    ActionConst,
    Overlay,
    Tabs,
    Modal,
    Drawer,
    Stack,
    Lightbox,
} from "react-native-router-flux"
import Ionicons from 'react-native-vector-icons/Ionicons'

import Icon from 'react-native-vector-icons/Ionicons';
import IconSimpleLine from 'react-native-vector-icons/SimpleLineIcons';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import IconEntypo from 'react-native-vector-icons/Entypo';
import IconFontAwesome5 from 'react-native-vector-icons/FontAwesome5'; 
import IconFontAwesome from 'react-native-vector-icons/FontAwesome'; 
import IconIonicons from 'react-native-vector-icons/Ionicons'; 


import Favorite from './view/favorite'
import Aktivitas from './view/Aktivitas'
import Pembayaran from './view/Pembayaran'
import Inbox from './view/Inbox'
import Home from './view/Beranda'
import Account from './view/Akun'
import SplashScreen from './view/splash'

import { COLOR_THEME } from './lib/color'

const getSceneStyle = () => ({
  backgroundColor: "#EEEEEE",
  shadowOpacity: 100,
  shadowRadius: 3
});

let exitCount = 0;

const onBackPress = () => {
  let route = Actions.state.routes;
  let topSection = route[route.length - 1]
  let section = topSection.routes;
  switch (route[route.length - 1].routeName) {
      case "_main":

          let tab = route[route.length - 1].routes[0].index;
          if(tab == 0) {
              exitCount++;
              ToastAndroid.showWithGravityAndOffset(
                  'Tekan lagi untuk keluar',
                  ToastAndroid.SHORT,
                  ToastAndroid.BOTTOM,
                  25,
                  100
              );
                  if(exitCount == 2) {
                      exitCount = 0
                      BackHandler.exitApp()
                  }
          }
          else {
              exitCount = 0;
              Actions.pop();     
          }

          return true;
          break;
  }
  
}

export default class App extends Component {

  /**
   * @function constructor
   * @param {object} props 
   */
  constructor(props) {
      super(props);
      this.state = {

      }
  }

  render() {
      return (
          <Router
              getSceneStyle={getSceneStyle}
              backAndroidHandler={onBackPress}
              >
              <Stack
                  hideNavBar
                  key="root"
                  titleStyle={{ alignSelf: 'center' }}                  
                >
                  <Scene key="_splash" component={SplashScreen} hideNavBar hideTabBar initial />
                  <Scene key="_main" hideNavBar type={ActionConst.REPLACE} panHandlers={null} >

                  <Tabs  
                      activeTintColor={COLOR_THEME}   
                      key="tabbar" 
                      tabBarPosition={'bottom'}
                      lazy={true}
                      labelStyle={{fontSize:9}}
                    >

                          <Stack                                
                              key="tab_home"
                              title="Beranda"
                              tabBarLabel="Beranda"
                              icon={_iconHome}
                              >
                              <Scene key="Home" component={Home} title="Beranda" initial hideNavBar />
                          </Stack>
                          
                          <Stack                                
                              key="tab_aktivitas"
                              title="Aktivitas"
                              tabBarLabel="Aktivitas"
                              icon={_iconActivitas}
                              >
                              <Scene key="Aktivitas"  component={Aktivitas} title="Aktivitas" initial hideNavBar />
                          </Stack>

                          <Stack                                
                              key="tab_pay"
                              title="Pembayaran"
                              tabBarLabel="Pembayaran"
                              icon={_iconPay}
                              >
                              <Scene key="Pembayaran"  component={Pembayaran} title="Pembayaran" initial hideNavBar />
                          </Stack>

                          <Stack                                
                              key="tab_inbox"
                              title="Kotak Masuk"
                              tabBarLabel="Kotak Masuk"
                              icon={_iconInbox}
                              >
                              <Scene key="Inbox"  component={Inbox} title="Kotak Masuk" initial hideNavBar />
                          </Stack>

                          <Stack                                
                              key="tab_account"
                              title="Akun"
                              tabBarLabel="Akun"
                              icon={_iconAccount}
                              >
                              <Scene key="account" component={Account} title="Akun" initial hideNavBar hideTabBar={false} />

                          </Stack>
                      </Tabs>
                  </Scene>

              </Stack>
          </Router>
      );
  }
}

let size = 20;
let top = 7 ;

function _iconHome(props) {
  return(
      <IconIonicons size={25}
          name={"compass"}
          color={props.focused ? COLOR_THEME : '#9aa69c' }
          style={{paddingTop:top}}
      />
  )
}

function _iconActivitas(props) {
    return(
        <IconMaterialIcons size={20}
            name={"my-library-books"}
            color={props.focused ? COLOR_THEME : '#9aa69c' }
            style={{paddingTop:top}}
            size={25}
        />
    )
}

function _iconPay(props) {
    return(
        <IconEntypo size={20}
            name={"wallet"}
            color={props.focused ? COLOR_THEME : '#9aa69c' }
            style={{paddingTop:top}}
            size={25}
        />
    )
}

function _iconInbox(props) {
    return(
        <IconIonicons size={20}
            name={"chatbox-ellipses"}
            color={props.focused ? COLOR_THEME : '#9aa69c' }
            style={{paddingTop:top}}
            size={25}
        />
    )
}

function _iconAccount(props) {
  return(
      <IconFontAwesome size={size}
          name={"user-circle"}
          color={props.focused ? COLOR_THEME : '#9aa69c' }
          style={{paddingTop:top}}
      />
  )
}

