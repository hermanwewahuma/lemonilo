import React, {Component} from 'react';
import {
    View,
    Image,
    StatusBar,
    Text,
    ToastAndroid,
    Platform
} from 'react-native';

import { COLOR_THEME } from '../../lib/color'

export default class Index extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    componentDidMount() {     
        if(Platform.OS === "android") {
            ToastAndroid.showWithGravityAndOffset(
                'Coming Soon',
                ToastAndroid.SHORT,
                ToastAndroid.BOTTOM,
                25,
                100
            );
        }else {
            Alert.alert(
                "INFO",
                "Coming Soon!",
                [
                    {text:"OKE", style:"cancel"}
                ]
            )
        }
    }

    render() {

        return (
            <View style={{ flex: 1, }}>
                <StatusBar
                    hidden = {false}
                    backgroundColor = {COLOR_THEME}
                    translucent = {false}
                    networkActivityIndicatorVisible = {true}
                />
                
            </View>
        );
    }
}