import React, {Component} from 'react';
import {
    View,
    Animated,
    Image,
    Easing,
    StatusBar,
    AsyncStorage,
    Text,
    Alert
} from 'react-native';
import { Actions } from 'react-native-router-flux';

const version = require('../../../package.json').version;

let animationTimeout;
let timer = 3000;

export default class Index extends Component {

    constructor(props) {
        super(props);
        this.animatedValue = new Animated.Value(0);
        this.springValue = new Animated.Value(0.3);
        this.state = {
            login: false,
            doneOnboarding:0
        }
    }

    componentDidMount() {   
        this.animate();

       setTimeout(()=>{
        Actions.tab_home();
       },3000)
    }

    animate () {
        this.animatedValue.setValue(0)
        Animated.timing(
          this.animatedValue,
          {
            toValue: 1,
            duration: 2000,
            easing: Easing.linear,
            useNativeDriver:true
          }
        ).start(() => this.animate())
      }


    render() {

        const opacity = this.animatedValue.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [0, 1, 0]
        })
        
        return (
            <View style={{ flex: 1, backgroundColor: '#ffff', justifyContent: 'center', alignItems: 'center' }}>
                <StatusBar
                    hidden = {false}
                    backgroundColor = {"#ffff"}
                    translucent = {false}
                    networkActivityIndicatorVisible = {true}
                />

            <Animated.View
                style={{opacity, height: 400, width: '80%',justifyContent: 'center', alignItems: 'center'}} >
                    <Animated.Image
                        style={{ height: 200, width:'60%'}}                        
                        source={require('../../assets/image/grab/grab.png')} 
                        resizeMode={'contain'}
                    />
            </Animated.View>
                <View style={{ width: '100%', height: 100, justifyContent: 'center', alignItems: 'center', position: 'absolute', bottom: 0 }}>
                    <Text small style={{ color: "#00b45e"}}>Version {version} </Text>
                </View>
            </View>
        );
    }
}