import React, {Component} from 'react';
import {
    View,
    Image,
    StatusBar,
    Text,
    StyleSheet,
    ScrollView,
    TouchableOpacity,
    Dimensions,
    FlatList,
    ActivityIndicator,
    RefreshControl,
    SafeAreaView,
    Alert,
    Platform,
    ToastAndroid,
} from 'react-native';

import { COLOR_THEME } from '../../lib/color'

import IconFeather from 'react-native-vector-icons/Feather'; 
import IconAntDesign from 'react-native-vector-icons/AntDesign'; 
import IconMaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'; 
import IconFontisto from 'react-native-vector-icons/Fontisto'; 
import IconIonicons from 'react-native-vector-icons/Ionicons'; 
import IconEntypo from 'react-native-vector-icons/Entypo'; 
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons'; 
import IconOcticons from 'react-native-vector-icons/Octicons'; 
import IconFontAwesome5 from 'react-native-vector-icons/FontAwesome5'; 

const { width } = Dimensions.get('window')

export default class Index extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading : true,
            refresh : false
        }
    }

    componentDidMount() {  
        setTimeout(()=>{
            this.setState({
                loading : false
            })
        }, 3000)
    }

    _warning=()=>{
        if(Platform.OS === "android") {
            ToastAndroid.showWithGravityAndOffset(
                'Coming Soon',
                ToastAndroid.SHORT,
                ToastAndroid.BOTTOM,
                25,
                100
            );
        }else {
            Alert.alert(
                "INFO",
                "Coming Soon!",
                [
                    {text:"OKE", style:"cancel"}
                ]
            )
        }
    }


    _onRefresh =()=>{
        this.setState({
            refresh : true
        })

        setTimeout(()=>{
            this.setState({
                refresh : false
            })
        }, 2000)
    }

    render() {

        const screenWidth = Math.round(Dimensions.get('window').width);

        return (
            <>
                <SafeAreaView>
                    <View style={{ backgroundColor : "#ffff" }}>
                        <StatusBar
                            hidden = {false}
                            backgroundColor = {COLOR_THEME}
                            translucent = {false}
                            networkActivityIndicatorVisible = {true}
                            inactiveSlideScale={0.99}
                            inactiveSlideOpacity={0.8}
                            enableMomentum={false}
                        />
                       
                        <ScrollView
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refresh}
                                    onRefresh={() => this._onRefresh()}
                                />
                              }
                        >
                            <View style={styles.header}>
                                <View style={styles.header1}>
                                    <View style={styles.barcode}>
                                        <TouchableOpacity style={styles.barcode1}
                                            onPress={()=>this._warning()}
                                        >
                                            <IconAntDesign 
                                                name="scan1"
                                                size={25}
                                            />
                                        </TouchableOpacity>
                                    </View>
                                    <TouchableOpacity style={styles.search}
                                        onPress={()=>this._warning()}
                                    >
                                        <IconFontAwesome5 
                                            name = {"search"}
                                            size ={15}
                                            color={"grey"}
                                        />
                                        <Text style={{color : "grey", marginLeft:5, fontWeight:"100"}}>Cari di aplikasi grab</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            {this.state.loading  ? <ActivityIndicator size="large" color={COLOR_THEME}/> : <View/> }

                            {/* 
                                ============================
                                        PAYMENT & POINT
                                ============================
                            */}

                            <View style={styles.pay}>
                                <TouchableOpacity style={styles.ovo}
                                    onPress={()=>this._warning()}
                                >
                                    <View style={styles.ovoImg}>
                                        <Image
                                            style={{ height: 30, width:20}}                        
                                            source={require('../../assets/image/grab/ovo.png')} 
                                            resizeMode={'contain'}
                                        />
                                    </View>
                                    <View style={styles.ovoPakai}>
                                        <Text>Pakai OVO</Text>
                                        <IconAntDesign 
                                            name="right"
                                            size={12}
                                            color={"#e3e8e4"}
                                            style={{left:10}}
                                        />
                                    </View>
                                </TouchableOpacity>
                                <View style={styles.line}></View>
                                <TouchableOpacity  style={styles.ovo}
                                    onPress={()=>this._warning()}
                                >
                                    <View style={styles.crown}>
                                        <IconMaterialCommunityIcons 
                                            name="crown"
                                            size={20}
                                            color={COLOR_THEME}
                                        />
                                    </View>
                                    <View style={styles.ovoPakai}>
                                        <Text>0 Point</Text>
                                        <IconAntDesign 
                                            name="right"
                                            size={12}
                                            color={"#e3e8e4"}
                                            style={{left:10}}
                                        />
                                    </View>
                                </TouchableOpacity>
                            </View>

                            <View style={styles.lineBottom}></View>
                            
                            {/* 
                                ============================
                                        MENU CONTENT
                                ============================
                            */}

                            <View style={styles.content}>
                                <View style={styles.content1}>
                                    <TouchableOpacity style={styles.food1}
                                        onPress={()=>this._warning()}
                                    >
                                        <View style={styles.food}>
                                            <IconIonicons 
                                                name ="fast-food"
                                                size={25}
                                            />
                                        </View>
                                        <Text style={styles.txt}>Makanan</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.food1}
                                        onPress={()=>this._warning()}
                                    >
                                        <View style={styles.food}>
                                            <IconEntypo 
                                                name={"shopping-cart"}
                                                size={25}
                                            />
                                        </View>                                        
                                        <Text style={styles.txt}>Mart</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.food1} 
                                        onPress={()=>this._warning()}
                                    >
                                        <View style={styles.food}>
                                            <IconEntypo 
                                                name={"box"}
                                                size={25}
                                            />
                                        </View>                                         
                                        <Text style={styles.txt}>Express</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.food1}
                                        onPress={()=>this._warning()}
                                    >
                                        <View style={styles.food}>
                                            <IconEntypo 
                                                name={"mobile"}
                                                size={25}
                                            />
                                        </View>
                                        <Text style={styles.txt}>Pulsa/Token</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={[styles.content1, { marginTop:20 }]}>
                                    <TouchableOpacity style={styles.food1}
                                        onPress={()=>this._warning()}
                                    >
                                        <View style={styles.food}>
                                            <IconMaterialCommunityIcons 
                                                name ="taxi"
                                                size={25}
                                            />
                                        </View>
                                        <Text style={styles.txt}>Mobil</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.food1}
                                        onPress={()=>this._warning()}
                                    >
                                        <View style={styles.food}>
                                            <IconFontisto 
                                                name ={"motorcycle"}
                                                size={25}
                                            />
                                        </View>
                                        <Text style={styles.txt}>Motor</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.food1}
                                        onPress={()=>this._warning()}
                                    >
                                        <View style={styles.food}>
                                            <IconMaterialIcons 
                                                name={"family-restroom"}
                                                size={25}
                                            />
                                        </View>
                                        <Text style={styles.txt}>Asuransi</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.food1}
                                        onPress={()=>this._warning()}
                                    >
                                        <View style={styles.food}>
                                            <IconOcticons 
                                                name={"kebab-horizontal"}
                                                size={25}
                                            />
                                        </View>
                                        <Text style={styles.txt}>Lainnya</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>

                            <View style={styles.lineBottom}></View>

                            {/* 
                                ============================
                                        BANNER
                                ============================
                            */}
                            
                            <TouchableOpacity style ={styles.banner}
                                onPress={()=>this._warning()}
                            >
                               <View style={styles.bannerImg}>
                                    <Image 
                                        style={{ height: 160, width:'100%', borderRadius :5}}                        
                                        source={require('../../assets/image/grab/banner.jpg')} 
                                        resizeMode={'cover'}
                                    />
                               </View>
                                <View style={styles.bannerDisc}>
                                    <Text style={{ fontWeight:"bold"}}>Promo diskon 30% untuk semua menu!</Text>
                                    <Text>Disponsori oleh kopi kenanga</Text>
                                </View>
                            </TouchableOpacity>

                            {/* 
                                ============================
                                        TANGGAL TUA
                                ============================
                            */}
                            <View style={styles.promoTglTua}>
                                <Text style={{ fontWeight:"bold", marginLeft:15, fontSize:18}}>Anti tanggal tua buat kamu</Text>
                            </View>
                            <View style ={styles.promo}>
                               <TouchableOpacity style={styles.promo50}
                                    onPress={()=>this._warning()}
                               >
                                    <Image 
                                        style={{ height: 200, width : "100%", borderRadius :15}}                        
                                        source={require('../../assets/image/grab/50.jpg')} 
                                        resizeMode={'cover'}
                                    />
                                    <View style={{marginLeft:10}} >
                                        <Text style={{ fontWeight:"bold"}}>Bertualang menu nikmat akhir bulan</Text>
                                        <View style={{flexDirection:"row", alignItems:"center", marginTop:5}}>
                                            <IconFontAwesome5 
                                                name = "calendar-day"
                                                color ={"grey"}
                                                size={12}
                                            />                                        
                                            <Text style={{color:"grey", fontSize:12, marginLeft:5}}>Sampai 21 Feb</Text>
                                        </View>
                                    </View>
                               </TouchableOpacity>
                               <TouchableOpacity style={styles.promo51}
                                    onPress={()=>this._warning()}
                               >                                    
                                    <Image 
                                        style={{ height: 200,width : "100%",  borderRadius :15}}                        
                                        source={require('../../assets/image/grab/51.jpg')} 
                                        resizeMode={"cover"}
                                    />
                                    <View style={{marginLeft:10}} >
                                        <Text style={{ fontWeight:"bold"}}>Promo diskon 30% untuk semua menu!</Text>
                                        <View style={{flexDirection:"row", alignItems:"center", marginTop:5}}>
                                            <IconFontAwesome5 
                                                name = "calendar-day"
                                                color ={"grey"}
                                                size={12}
                                            />                                        
                                            <Text style={{color:"grey", fontSize:12, marginLeft:5 }}>Sampai 21 Feb</Text>
                                        </View>
                                    </View>
                               </TouchableOpacity>
                            </View>

                        </ScrollView>
                    </View>
                </SafeAreaView>
            </>
        );
    }
}


const styles = StyleSheet.create({
    header : {
        backgroundColor : COLOR_THEME,
        padding:10,
        height : 65
    },
    header1 : {
        backgroundColor : "#ffff",
        height : 40,
        margin :3,
        // borderRadius : 5,
        elevation:3,
        flexDirection : "row"
    },

    barcode : {
        backgroundColor : "#e3e8e4",
        // justifyContent :"center",
        alignItems:"center",
    },
    barcode1 : {
        padding :10,
        marginTop:-3
    },
    search :{
        flexDirection : "row",
        // backgroundColor :"red",
        justifyContent:"center",
        alignItems:"center",
        width:"87%"
    },

    pay : {
        flexDirection :"row",
        justifyContent : "space-around",
        alignItems:"center",
        paddingTop:10,
        paddingBottom :10
    },
    ovo : {
        flexDirection :"row"
    },
    ovoImg : {
        borderWidth:2,
        borderRadius :50,
        paddingRight:5,
        paddingLeft:5,
        borderColor : "#e3e8e4", 
        marginRight:5
    },
    crown : {
        borderWidth:2,
        borderRadius :50,
        paddingRight:5,
        paddingLeft:5,
        paddingTop:4,
        paddingBottom:4,
        borderColor : "#e3e8e4", 
        marginRight:5
    },
    ovoPakai : {
        flexDirection : "row",
        alignItems:"center",
    },
    line : {
        borderRightWidth :1,
        borderRightColor : "#e3e8e4",
        height:"100%"
    },
    lineBottom : {
        borderBottomWidth:1,
        borderBottomColor : "#e3e8e4"
    },

    content : {
        paddingTop:18,
        paddingBottom :18
    },
    content1 : {
        flexDirection : "row",
        justifyContent : "space-around",
    },
    food1 :{       
        justifyContent:"center",
        alignItems:"center",
        width : "25%",
    },
    food : {
        width : 50,
        borderWidth:1,
        borderRadius:50,
        borderColor : "#e3decf",
        backgroundColor : "#b8e6bb",
        alignItems:"center",
        paddingTop:10,
        paddingBottom :10
    },
    txt : {
        fontSize :12
    },
    banner : {
        marginTop:20
    },
    bannerDisc : {
        marginLeft :15
    },
    bannerImg : {
        // backgroundColor :"red"
        marginLeft:10,
        marginRight:10
    },

    promoTglTua : {
        marginTop:20, 
        marginBottom:20
    },
    promo :{
        flexDirection : "row",
        marginBottom:50,
        // marginLeft:10,
        // marginRight:10
    },
    promo50 :{
        // backgroundColor : "red",
        width :"50%",
        height : 250,
        paddingRight:5,
        paddingLeft:10
    },
    promo51 : {
        width :"50%",
        height : 250,
        paddingRight:10,
        paddingLeft:5
    }
});